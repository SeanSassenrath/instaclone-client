import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './post.less';

export class Post extends Component {

  render() {
    const { author, description, img, title } = this.props;
    return (
      <div className="post-container">
        <div className="post-info">
          <div className="author-container">
            <span className="avatar-placeholder" style={{backgroundColor: '#'+(Math.random()*0xFFFFFF<<0).toString(16)}}></span>
            <h1 className="title">{author}</h1>
          </div>
        </div>
        <div>
          <img src={img} alt={title} className="post-img" />
        </div>
        <div className="post-info">
          <h1 className="title">{title}</h1>
          <p className="description">{description}</p>
        </div>
      </div>
    )
  }
}

Post.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string
}