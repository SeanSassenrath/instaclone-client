import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import qs from 'qs';
import './modal-form.less';

export class ModalForm extends Component {

  constructor() {
    super();
    this.state = {
      author: '',
      title: '',
      description: '',
      imgUrl: ''
    }
  }

  modalFormStyles = () => {
    const { visible } = this.props;
    return visible ? "modal-form-backdrop" : "modal-form-backdrop hidden"
  }

  setInputState = (e) => {
    const inputName = e.target.getAttribute('data-name');
    this.setState(...this.state, {[inputName]: e.target.value});
  }

  submitForm = () => {
    axios.post(
      'https://secure-eyrie-25288.herokuapp.com/api/posts',
      qs.stringify(this.state)
    )
    .then(resp => console.log('response', resp))
    .catch(err => console.log('err', err))
  }

  render() {
    const { author, title, description, imgUrl } = this.state;
    return (
      <div className={this.modalFormStyles()}>
        <div className="form-card">
          <form action="" className="form-container">
            <input 
              data-name="author"
              type="text"
              value={author} 
              onChange={e => this.setInputState(e)}
              placeholder="Author *"
              className="form-input"
              required
            />
            <input
              data-name="title"
              type="text" 
              value={title} 
              onChange={e => this.setInputState(e)}
              placeholder="Title *" 
              className="form-input"
              required
            />
            <input
              data-name="description"
              type="text" 
              value={description}
              onChange={e => this.setInputState(e)}
              placeholder="Description" 
              className="form-input" 
            />
            <input 
              data-name="imgUrl"
              type="text" 
              value={imgUrl}
              onChange={e => this.setInputState(e)}
              placeholder="Image URL *" 
              className="form-input"
              required
            />
            <input 
              type="submit"
              value="Submit post"
              className="form-submit"
              onClick={this.submitForm}
            />
          </form>
        </div>
      </div>
    )
  }
}