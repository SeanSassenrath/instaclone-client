import React, { Component } from 'react';
import ReactDOm from 'react-dom';
import { Post } from './post/post';
import { ModalForm } from './modal-form/modal-form';
import axios from 'axios';
import './app.less';

export class App extends Component {

  constructor() {
    super();
    this.state = {
      posts: [],
      modalFormVisible: false
    }
  }

  componentDidMount() {
    axios.get('https://secure-eyrie-25288.herokuapp.com/api/posts')
      .then(posts => {
        console.log('posts', posts.data)
        posts.data.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
        this.setState({posts: posts.data})
      })
  }

  toggleModalForm = () => {
    this.setState(prevState => ({ modalFormVisible: !prevState.modalFormVisible }));
  }

  renderPosts = () => {
    const { posts } = this.state;
    return posts.map((post, i) => (
      <Post img={post.imgUrl} title={post.title} author={post.author} description={post.description} key={i} />
    ))
  }

  render() {
    const { modalFormVisible, posts } = this.state;
    return (
      <div className='app-container'>
        <nav className='nav'>
          <h1 className='title'>
            Instaclone
          </h1>
          <button onClick={this.toggleModalForm} className="modal-toggle">{`${modalFormVisible ? 'Cancel' : 'Add'} post`}</button>
        </nav>
        <ModalForm visible={modalFormVisible} />
        <div className={'posts-container'}>
          {this.renderPosts()}
        </div>
      </div>
    )
  }
}